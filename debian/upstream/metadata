Name: CP2K
Homepage: http://www.cp2k.org
Other-References: http://manual.cp2k.org/trunk/references.html
Repository-Browse: http://sourceforge.net/p/cp2k/code/HEAD/tree/
Reference:
 - Title: "CP2K version 2.6.1, the CP2K developers group: http://www.cp2k.org"
   Year: 2015
   URL: http://www.cp2k.org
 - Author: Jürg Hutter and Marcella Iannuzzi and Florian Schiffmann and Joost VandeVondele
   Title: "CP2K: atomistic simulations of condensed matter systems"
   Journal: Comp. Mol. Sci.
   Year: 2013
   Volume: 4
   Number: 1
   Pages: 15-25
   DOI: 10.1002/wcms.1159
   URL: http://onlinelibrary.wiley.com/doi/10.1002/wcms.1159/
   eprint: http://onlinelibrary.wiley.com/doi/10.1002/wcms.1159/pdf
 - Author: J. VandeVondele and M. Krack and F. Mohamed and M. Parrinello and T. Chassaing and J. Hutter
   Title: "QUICKSTEP: Fast and accurate density functional calculations using a mixed Gaussian and plane waves approach"
   Journal: Comp. Phys. Comm.
   Year: 2005
   Volume: 167
   Number: 2
   Pages: 103-128
   DOI: 10.1016/j.cpc.2004.12.014
   URL: http://www.sciencedirect.com/science/article/pii/S0010465505000615
 - Author: Thomas D Kuhne and Matthias Krack and Fawzi R Mohamed and Michele Parrinello
   Title: "Efficient and accurate Car-Parrinello-like approach to Born-Oppenheimer molecular dynamics"
   Journal: Phys. Rev. Lett.
   Year: 2007
   Volume: 98
   Number: 6
   Pages: 066401
   DOI: 10.1103/PhysRevLett.98.066401
   PMID: 17358962
   URL: http://prl.aps.org/abstract/PRL/v98/i6/e066401
   eprint: http://prl.aps.org/pdf/PRL/v98/i6/e066401
 - Author: Gerald Lippert and Jurg Hutter and Michele Parrinello
   Title: "A hybrid Gaussian and plane wave density functional scheme"
   Journal: Mol. Phys.
   Year: 1997
   Volume: 92
   Number: 3
   Pages: 447-487
   DOI: 10.1080/002689797170220
   URL: http://www.tandfonline.com/doi/abs/10.1080/002689797170220
   eprint: http://www.tandfonline.com/doi/pdf/10.1080/002689797170220
 - Author: Gerald Lippert and Jurg Hutter and Michele Parrinello
   Title: "The Gaussian and augmented-plane-wave density functional method for ab initio molecular dynamics simulations"
   Journal: Theo. Chem. Acc.
   Year: 1999
   Volume: 103
   Number: 2
   Pages: 124-140
   DOI: 10.1007/s002140050523
   URL: http://www.springerlink.com/content/7f52yxb1qy4nqawb/
   eprint: http://www.springerlink.com/content/7f52yxb1qy4nqawb/fulltext.pdf
 - Author: Manuel Guidon and Florian Schiffmann and Jürg Hutter and Joost VandeVondele
   Title: "Ab initio molecular dynamics using hybrid density functionals"
   Journal: J. Chem. Phys.
   Year: 2008
   Volume: 128
   Number: 21
   Pages: 214104
   DOI: 10.1063/1.2931945
   URL: http://jcp.aip.org/resource/1/jcpsa6/v128/i21/p214104_s1
 - Author: Matthias Krack and Michele Parrinello
   Title: "All-electron ab-initio molecular dynamics"
   Journal: Phys. Chem. Chem. Phys.
   Year: 2000
   Volume: 2
   Number: 10
   Pages: 2105-2112
   DOI: 10.1039/b001167n
   URL: http://pubs.rsc.org/en/Content/ArticleLanding/2000/CP/b001167n
   eprint: http://pubs.rsc.org/en/content/articlepdf/2000/cp/b001167n
